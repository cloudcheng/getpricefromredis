package com.systex;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

public class ParseHandler extends DefaultHandler {


	boolean bid;
	boolean bdealprice;
	boolean bshortname;
	
	public Element priceinfo;
	private Document document;

    public Element getPriceinfo() {
		return priceinfo;
	}

	public void setPriceinfo(Element priceinfo) {
		this.priceinfo = priceinfo;
	}

	@Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {
        if (qName.equalsIgnoreCase("Symbol")){
        	document = DocumentHelper.createDocument();
        	priceinfo = document.addElement("Symbol");
        	priceinfo.addAttribute("id", attributes.getValue("id"));
        	priceinfo.addAttribute("dealprice", attributes.getValue("dealprice"));
        	priceinfo.addAttribute("shortname", attributes.getValue("shortname"));
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {


    }
}
