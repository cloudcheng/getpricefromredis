package com.systex;

import org.dom4j.Element;

public class PriceInfo {
	public Element  info;

	public Element getInfo() {
		return info;
	}
	public void setInfo(Element info) {
		this.info = info;
	}
	public long getActivetime() {
		return activetime;
	}
	public void setActivetime(long activetime) {
		this.activetime = activetime;
	}
	public long activetime;
}
