package com.systex;


public class FMSTMBInfo {
    public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getCprice() {
		return cprice;
	}
	public void setCprice(String cprice) {
		this.cprice = cprice;
	}
	
	public String getExhno() {
		return exhno;
	}
	public void setExhno(String exhno) {
		this.exhno = exhno;
	}
	
	public String stock;
	public String cname;
	public String cprice;
	public String exhno;

	
}

