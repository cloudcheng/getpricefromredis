package com.systex;

import java.util.*;



import java.lang.*;
import java.nio.charset.StandardCharsets;
import java.io.*;
import java.util.Hashtable;
import java.util.regex.*;

import redis.clients.jedis.Jedis;




import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.*;


import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.log4j.*;

public class SGTPPrice {

    public static java.util.HashMap<String, MSTMBInfo> htMSTMB=new java.util.HashMap<String, MSTMBInfo>();
    public static java.util.HashMap<String, FMSTMBInfo> htFMSTMB=new java.util.HashMap<String, FMSTMBInfo>();
    public static java.util.Hashtable<String, MIEXHInfo> htMIEXH=new java.util.Hashtable<String, MIEXHInfo>();
    public static java.util.Hashtable<String, MSYSInfo> htMSYS=new java.util.Hashtable<String, MSYSInfo>();
    public static java.util.Hashtable<String, MAPPINGLISTInfo> htMAPPINGList=new java.util.Hashtable<String, MAPPINGLISTInfo>();
    
    public static Hashtable<String, PriceInfo> htPriceInfo=new Hashtable<String, PriceInfo>();
    
    public static java.util.List<String> priceChangeList=new java.util.ArrayList<String>();
    
    private static Logger logger = Logger.getLogger(SGTPPrice.class);
    private static Logger dayLogger=org.apache.log4j.Logger.getLogger("DAY");
    
    
    public static void init(){
    	try{
	    	Conf conf=new Conf();
	    	conf.load();
	    	
	  
	    	
	    	dayLogger.info("MARKET_TW_STOCK_ENABLE="+ Conf.MARKET_TW_STOCK_ENABLE);
	    	logger.info("MARKET_TW_STOCK_ENABLE="+ Conf.MARKET_TW_STOCK_ENABLE);
	    	if (Conf.MARKET_TW_STOCK_ENABLE.equals("Y")){
	    		dayLogger.info("load TW Stock");
	    		loadStmbInfo();
	    	}
	    	
	    	dayLogger.info("MARKET_FOREIGN_STOCK_ENABLE="+ Conf.MARKET_FOREIGN_STOCK_ENABLE);
	    	logger.info("MARKET_FOREIGN_STOCK_ENABLE="+ Conf.MARKET_FOREIGN_STOCK_ENABLE);
	    	if (Conf.MARKET_FOREIGN_STOCK_ENABLE.equals("Y")){
	    		dayLogger.info("load Foreign Stock");
	    		loadFStmbInfo();
	    		loadMIEXHInfo();
		    	loadMSYSInfo();
		    	loadMAPPINGLISTInfo();
	    	}
	    	
	    	logger.info("init success");
	    	dayLogger.info("init success" );
	    	
	    	DateFormat df = new SimpleDateFormat("yyyyMMdd");
	    	Calendar cal=Calendar.getInstance();
	   
	        
	        for(int i=0;i < conf.MAX_IDLE;i++){
	        	Jedis jedis = RedisPoolManager.getResource();
	        	jedis.close();
	        }
        }
    	catch(Exception ex){
    		logger.error(ex);
    		dayLogger.error(ex);
    	}
    	
    }
    
   
    //載入國內證商品
    private static void loadStmbInfo(){
    	Jedis jedis;
    	
    	String connectionString =  "";
                

            // Declare the JDBC objects.  
	    	Connection connection = null;  
	        Statement statement = null;   
	        ResultSet rs = null;  
	        java.util.HashMap<String, MSTMBInfo> httempMSTMB=new java.util.HashMap<String, MSTMBInfo>();    
            try {
            	
            	if (Conf.DB_DRIVER_TYPE.equals("MSSQL")){
            		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            		connectionString="jdbc:sqlserver://"+ Conf.DB_IP+":"+Conf.DB_PORT+";" 
                            + "database="+ Conf.DB_NAME+";"  
                            + "user="+Conf.DB_USER+";"  
                            + "password="+Conf.DB_PWD+";"  
                            + "loginTimeout=30;";  
            	}
            	else if (Conf.DB_DRIVER_TYPE.equals("MARIADB")){
            		Class.forName("org.mariadb.jdbc.Driver");
            		connectionString="jdbc:mariadb://"+ Conf.DB_IP+":"+Conf.DB_PORT+"/"+Conf.DB_NAME+"?" 
                            + "user="+Conf.DB_USER+"&"  
                            + "password="+Conf.DB_PWD+"&"  
                            + "connectTimeout=30";  
            	}
            	else{
            		logger.error("NO MATCH DB_DRIVER_TYPE");
            		return;
            	}
            	
                connection = DriverManager.getConnection(connectionString);  
                
                String selectSql = "SELECT STOCK, CNAME, MTYPE, CPRICE, TEDATE FROM MSTMB";  
                statement = connection.createStatement();  
                rs = statement.executeQuery(selectSql); 
                while(rs.next()){
                	MSTMBInfo mstmb=new MSTMBInfo();
                	String key = rs.getString("STOCK");
                	mstmb.setCname(rs.getString("CNAME"));
                	mstmb.setStock(rs.getString("STOCK"));
                	mstmb.setMtype(rs.getString("MTYPE"));
                	String tedate=rs.getString("TEDATE");
                    String cprice = rs.getString("CPRICE");
                    
                    //商品下市
                    if (tedate != null && !tedate.equals("")){
                    	DateFormat df = new SimpleDateFormat("yyyyMMdd");
                    	Calendar cal=Calendar.getInstance();
                        String s = df.format(cal.getTime());
                        if (s.compareTo(tedate)>=0){
                        	cprice = "0";
                        }
                    }
                    
                	mstmb.setCprice(cprice);
                	if (!httempMSTMB.containsKey(key)){
                		httempMSTMB.put(key, mstmb);
                	}
                }
                
                if (httempMSTMB.size()>0){
                	htMSTMB = httempMSTMB;
                }

            }  
            catch (Exception ex) {  
                ex.printStackTrace();
                logger.error(ex);
        		dayLogger.error(ex);
            }  
            finally {  
            	if (rs!=null){
	            	try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
            	if (statement!=null){
            		try {
						statement.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
                if (connection != null) try { connection.close(); } catch(Exception e) {}  
            }  

    }

    //載入國外證商品
    private static void loadFStmbInfo(){
    	String connectionString =  
                "jdbc:sqlserver://"+ Conf.DB_IP+":"+Conf.DB_PORT+";"  
                + "database="+ Conf.FDB_NAME+";"  
                + "user="+Conf.DB_USER+";"  
                + "password="+Conf.DB_PWD+";"  
                + "loginTimeout=30;";  

            // Declare the JDBC objects.  
	    	Connection connection = null;  
	        Statement statement = null;   
	        ResultSet rs = null;  
	        java.util.HashMap<String, FMSTMBInfo> httempFMSTMB=new java.util.HashMap<String, FMSTMBInfo>();

            try {
            	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                connection = DriverManager.getConnection(connectionString);  
                
                String selectSql = "SELECT STOCK, EXHNO, CPRICE, CNAME FROM MSTMB";  
                statement = connection.createStatement();  
                rs = statement.executeQuery(selectSql); 
                while(rs.next()){
                	FMSTMBInfo fmstmb=new FMSTMBInfo();
                	String key = rs.getString("EXHNO").trim()+rs.getString("STOCK").trim();
                	fmstmb.setCname(rs.getString("CNAME").trim());
                	fmstmb.setStock(rs.getString("STOCK").trim());
                	fmstmb.setExhno(rs.getString("EXHNO").trim());
                	fmstmb.setCprice(rs.getString("CPRICE"));
                	if (!httempFMSTMB.containsKey(key)){
                		httempFMSTMB.put(key, fmstmb);
                	}
                }
                if (httempFMSTMB.size()>0){
                	htFMSTMB=httempFMSTMB;
                }
            }  
            catch (Exception ex) {  
                ex.printStackTrace();  
                logger.error(ex);
        		dayLogger.error(ex);
            }  
            finally {  
            	if (rs!=null){
	            	try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
            	if (statement!=null){
            		try {
						statement.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
                if (connection != null) try { connection.close(); } catch(Exception e) {}  
            }  

    }
    
    //載入國外證交易所檔
    private static void loadMIEXHInfo(){
    	logger.info("loadMIEXHInfo ");
    	String connectionString =  
                "jdbc:sqlserver://"+ Conf.DB_IP+":"+Conf.DB_PORT+";"  
                + "database="+ Conf.FDB_NAME+";"  
                + "user="+Conf.DB_USER+";"  
                + "password="+Conf.DB_PWD+";"  
                + "loginTimeout=30;";  

            // Declare the JDBC objects.  
	    	Connection connection = null;  
	        Statement statement = null;   
	        ResultSet rs = null;  
	        java.util.Hashtable<String, MIEXHInfo> httempMIEXH=new java.util.Hashtable<String, MIEXHInfo>();    
            try {
            	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                connection = DriverManager.getConnection(connectionString);  
                
                String selectSql = "SELECT EXHNO, MTYPE FROM MIEXH";  
                statement = connection.createStatement();  
                rs = statement.executeQuery(selectSql); 
                while(rs.next()){
                	MIEXHInfo miexh=new MIEXHInfo();
                	String exhno = rs.getString("EXHNO").trim();
                	miexh.setExhno(exhno);
                	miexh.setMtype(rs.getString("MTYPE").trim());
                	if (!httempMIEXH.containsKey(exhno)){
                		httempMIEXH.put(exhno, miexh);
                	}
                }
                
                if (httempMIEXH.size()>0){
                	htMIEXH = httempMIEXH;
                }

            }  
            catch (Exception ex) {  
            	ex.printStackTrace();
                logger.error(ex);
        		dayLogger.error(ex);
            }  
            finally {  
            	if (rs!=null){
	            	try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
            	if (statement!=null){
            		try {
						statement.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
                if (connection != null) try { connection.close(); } catch(Exception e) {}  
            }  

    }
    
    
    //載入系統參數
    private static void loadMSYSInfo(){
    	String connectionString =  
                "jdbc:sqlserver://"+ Conf.DB_IP+":"+Conf.DB_PORT+";"  
                + "database="+ Conf.DB_NAME+";"  
                + "user="+Conf.DB_USER+";"  
                + "password="+Conf.DB_PWD+";"  
                + "loginTimeout=30;";  

            // Declare the JDBC objects.  
	    	Connection connection = null;  
	        Statement statement = null;   
	        ResultSet rs = null;  
	        java.util.Hashtable<String, MSYSInfo> httempMSYS=new java.util.Hashtable<String, MSYSInfo>();      
            try {
            	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                connection = DriverManager.getConnection(connectionString);  
                
                String selectSql = "SELECT VARNAME, NUMBER, VALUE FROM MSYS WHERE VARNAME LIKE 'QUOTE%'";  
                statement = connection.createStatement();  
                rs = statement.executeQuery(selectSql); 
                while(rs.next()){
                	MSYSInfo msys=new MSYSInfo();
                	String varname = rs.getString("VARNAME").trim();
                	String number = rs.getString("NUMBER").trim();
                	String value = rs.getString("VALUE").trim();
                	String key = varname+number;
                	
                	msys.setNumber(number);
                	msys.setValue(value);
                	msys.setVarname(varname);
                	
                	if (!httempMSYS.containsKey(key)){
                		httempMSYS.put(key, msys);
                	}
                }
                
                if (httempMSYS.size()>0){
                	htMSYS = httempMSYS;
                }

            }  
            catch (Exception ex) {  
                ex.printStackTrace();
                logger.error(ex);
        		dayLogger.error(ex);
            }  
            finally {  
            	if (rs!=null){
	            	try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
            	if (statement!=null){
            		try {
						statement.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
                if (connection != null) try { connection.close(); } catch(Exception e) {}  
            }  
    }
//    public static String getPrice(String[] stocks){
//    	
//		String result = "";
//        long st=System.currentTimeMillis();
//		Document outdoc = DocumentHelper.createDocument();
//		Element outroot = outdoc.addElement("Symbols");
//		
//		for (int i = 0; i < stocks.length; i++) {
//
//			// check cache value
//			Element cahchValue = getCacheInfo(stocks[i]);
//			if (cahchValue != null) {
//				Element outelement = outroot.addElement("Symbol");
//				outelement.addAttribute("id", cahchValue.attributeValue("id"));
//				outelement.addAttribute("dealprice", cahchValue.attributeValue("dealprice"));
//				outelement.addAttribute("shortname", cahchValue.attributeValue("shortname"));
//			} else {
//				String id = stocks[i];
//				String dealprice = "0";
//				String shortname = id;
//				if (htMSTMB.containsKey(stocks[i])) {
//					MSTMBInfo mstmb = htMSTMB.get(stocks[i]);
//					dealprice = mstmb.getCprice();
//					shortname = mstmb.getCname();
//				}
//
//				Element outelement = outroot.addElement("Symbol");
//				outelement.addAttribute("id", id);
//				outelement.addAttribute("dealprice", dealprice);
//				outelement.addAttribute("shortname", shortname);
//			}
//
//		}
//		long end=System.currentTimeMillis();
//		
//		System.out.println(end - st);
//		return result = outdoc.asXML();
//    }
    

//    public static String getPrice(String[] stocks){
//    	String result ="";
//    	Calendar cal=Calendar.getInstance();
//    	long st=System.currentTimeMillis();
//    	Jedis jedis=RedisPoolManager.getResource();
//    	
//    	//作為檢核redis查回資料用,ht個數不為0代表無法從redis查回資料,需從預載商品取得
//    	HashMap<String, String> htcheckstock=new HashMap<String, String>();
//    	
//    	HashMap<String, MSTMBInfo> htoutstock=new HashMap<String, MSTMBInfo>();
//    	
//    	Document outdoc = DocumentHelper.createDocument();
//    	Element  outroot = outdoc.addElement("Symbols"); 
//    	long st1=System.currentTimeMillis();
//    	if (jedis!=null){
//    	    java.util.List<String> stocksList=new java.util.ArrayList<String>();
//    	    		
//			for(int i=0;i< stocks.length;i++){
//				String ixgwmtype = "1";
//		
//				
//				if (htMSTMB.containsKey(stocks[i])){
//					MSTMBInfo info = htMSTMB.get(stocks[i]);
//					htoutstock.put(stocks[i], info);
//					String mtype = info.getMtype();
//				    ixgwmtype = (mtype.equals("T") || mtype.equals("O")) ? "1":"8";
//				}
//				
//				String keys=ixgwmtype+","+stocks[i];
//				
//				htcheckstock.put(stocks[i], "1");
//				stocksList.add(keys);
//			}
//			
//					
//			
//			List<String> values=jedis.mget(stocksList.toArray(new String[stocksList.size()]));
//			jedis.close();
//			
//			Pattern p=Pattern.compile("shortname=\"([\\w\\W]*)\" id=\"(\\w+)\" dealprice=\"([\\d\\.]*)\"");
//			
//			for (String s : values) {
//				if (s == null)
//					continue;
//				Matcher m = p.matcher(s);
//				if (m.find()) {
//
//					String id = m.group(2);
//					String dealprice = m.group(3);
//					String shortname = m.group(1);
//
//					if (htoutstock.get(id)!=null){
//						MSTMBInfo info= htoutstock.get(id);
//						info.setCprice(dealprice);
//						htoutstock.replace(id, info);
//					}
//					
//					
//					
//					
////					outelement.addAttribute("id", id);
////					outelement.addAttribute("dealprice", dealprice);
////					outelement.addAttribute("shortname", shortname);
//					if (htcheckstock.containsKey(id)) {
//						htcheckstock.remove(id);
//					}
//				}
//
//			}
//
//			for (String s : htoutstock.keySet()) {
//				MSTMBInfo info= htoutstock.get(s);
//				Element outelement = outroot.addElement("Symbol");
//				outelement.addAttribute("id", info.getStock());
//				outelement.addAttribute("dealprice", info.getCprice());
//				outelement.addAttribute("shortname", info.getCname());
//			}
//			
//			
////			無法從redis查回資料,需從預載商品取得
//			for (String s: htcheckstock.keySet()) {
//				String id= s;
//				String dealprice = "0";
//				String shortname = s;
//				Element outelement = outroot.addElement("Symbol");
//				if (htoutstock.containsKey(s)){
//					MSTMBInfo info= htoutstock.get(s);
//					id = s;
//					shortname = info.getCname();
//					dealprice=  info.getCprice();
//				}
//				outelement.addAttribute("id", id);
//				outelement.addAttribute("dealprice", dealprice);
//				outelement.addAttribute("shortname", shortname);
//			}
//			
//    	}
//    	long ed=System.currentTimeMillis();
//    	if ( (ed-st1)>10){
//    		System.out.println((st1-st) +" "+ (ed-st)+ " "+ (ed-st1));
//    	}
//    	
//    	return result = outdoc.asXML();
//    }
    
    public static String getPrice(String[] stocks){
    	String result ="";
    	Calendar cal=Calendar.getInstance();
    	long st=System.currentTimeMillis();
    	Jedis jedis=RedisPoolManager.getResource();
    	
    	//作為檢核redis查回資料用,ht個數不為0代表無法從redis查回資料,需從預載商品取得
    	Hashtable<String, String> htcheckstock=new Hashtable<String, String>();
    	
    	Document outdoc = DocumentHelper.createDocument();
    	Element  outroot = outdoc.addElement("Symbols"); 
    	long st1=0;
    	
    	if (jedis!=null){
    		st1=System.currentTimeMillis();
        	    java.util.List<String> stocksList=new java.util.ArrayList<String>();
        	    		
    			for(int i=0;i< stocks.length;i++){
    				String ixgwmtype = "1";
    		
    				
    				if (htMSTMB.containsKey(stocks[i])){
    					String mtype = htMSTMB.get(stocks[i]).getMtype();
    					mtype = (mtype==null)? "T": mtype;
    				    ixgwmtype = (mtype.equals("T") || mtype.equals("O")) ? "1":"8";
    				}
    				
    				String keys=ixgwmtype+","+stocks[i];
    				
    				htcheckstock.put(stocks[i], "1");
    				stocksList.add(keys);
    	    	}
    			
    			
    			java.util.List<String> values=jedis.mget(stocksList.toArray(new String[stocksList.size()]));
    			jedis.close();
    			
    			Pattern p=Pattern.compile("shortname=\"([^\"]*)\" id=\"([^\"]*)\" dealprice=\"([^\"]*)\"");
    			
    			for (String s : values) {
    				if (s == null)
    					continue;
    				Matcher m = p.matcher(s);
    				if (m.find()) {

    					String id = m.group(2);
    					String dealprice = m.group(3);
    					String shortname = m.group(1);
    					String mtype="";
    					
    					if (htMSTMB.containsKey(id)) {
    						shortname = htMSTMB.get(id).cname;
    						mtype=htMSTMB.get(id).mtype;
    					}
    					
    					if (dealprice == null || dealprice.trim().equals("")) {
    						continue;
    					}
    					
    					Element outelement = outroot.addElement("Symbol");
    					outelement.addAttribute("id", id);
    					outelement.addAttribute("dealprice", dealprice);
    					outelement.addAttribute("shortname", shortname);
    					outelement.addAttribute("mtype", mtype);
    					if (htcheckstock.containsKey(id)) {
    						htcheckstock.remove(id);
    					}
    				}

    			}
    			
    			//無法從redis查回資料,需從預載商品取得
    			for (Enumeration e=htcheckstock.keys(); e.hasMoreElements();) {
    				String nextKey = (String) e.nextElement();
    				String id = nextKey;
    				String dealprice = "0";
    				String mtype="";
    				String shortname = id;
    				Element outelement = outroot.addElement("Symbol");
    				if (htMSTMB.containsKey(nextKey)){
    					id = nextKey;
    					shortname = htMSTMB.get(id).cname;
    					dealprice= htMSTMB.get(id).cprice;
    					mtype=htMSTMB.get(id).mtype;
    				}
    				outelement.addAttribute("id", id);
    				outelement.addAttribute("dealprice", dealprice);
    				outelement.addAttribute("shortname", shortname);
    				outelement.addAttribute("mtype", mtype);
    			}

			
    	}
    	else{
    		logger.error("NO JEDIS CONNECTION");
    		dayLogger.error("NO JEDIS CONNECTION");
    	}
    	long ed=System.currentTimeMillis();
    	return result = outdoc.asXML();
    }
    
    public static String getForeignPrice(String[] stocks){
    	String result ="";
    	Calendar cal=Calendar.getInstance();
    	long st=System.currentTimeMillis();
    	Jedis jedis=RedisPoolManager.getResource();
    	
    	
    	//作為檢核redis查回資料用,ht個數不為0代表無法從redis查回資料,需從預載商品取得
    	Hashtable<String, String> htcheckstock=new Hashtable<String, String>();
    	
    	//供存放商品推得交易所
    	Hashtable<String, List<String>> htstockexh =new Hashtable<String, List<String>>();
    	
    	//供存ix商品(需作轉換高品id)推得交易所
    	Hashtable<String, MAPPINGLISTInfo> htstockixmtypetoexh =new Hashtable<String,MAPPINGLISTInfo>();
    	
    	Document outdoc = DocumentHelper.createDocument();
    	Element  outroot = outdoc.addElement("Symbols"); 
    	if (jedis!=null){
    		
    	    java.util.List<String> stocksList=new java.util.ArrayList<String>();
	
			for(int i=0;i< stocks.length;i++){
				String ixgwmtype = "2";
				String[]  stockarr=stocks[i].split("\\|");
				String mtype= htMIEXH.get(stockarr[1]).getMtype();
				ixgwmtype=htMSYS.get("QUOTE"+ mtype.toUpperCase()+"0").getValue();
				
			    if (ixgwmtype==null){
			    	ixgwmtype="99";
			    }
			    
				String keys = ixgwmtype+","+stockarr[0];
				if (htcheckstock.containsKey(stocks[i]))
					continue;
						
				htcheckstock.put(stocks[i], "1");
				
				if (htMAPPINGList.containsKey(stockarr[1]+stockarr[0])){
					MAPPINGLISTInfo mappinglistInfo=htMAPPINGList.get(stockarr[1]+stockarr[0]);
					if (!htstockixmtypetoexh.containsKey(mappinglistInfo.getStockmapping().trim()))
					    htstockixmtypetoexh.put(mappinglistInfo.getStockmapping().trim(),mappinglistInfo);
					keys = ixgwmtype+","+mappinglistInfo.getStockmapping();
				}
				stocksList.add(keys);
				
				
				if (htstockexh.containsKey(stockarr[0])){
					htstockexh.get(stockarr[0]).add(stockarr[1]);
				}
				else{
					List<String> listexh = new ArrayList<String>();
					listexh.add(stockarr[1]);
					htstockexh.put(stockarr[0], listexh);
				}
				
	    	}
			
			
			java.util.List<String> values=jedis.mget(stocksList.toArray(new String[stocksList.size()]));
			jedis.close();
			
			Pattern p=Pattern.compile("shortname=\"([^\"]*)\" id=\"([^\"]*)\" dealprice=\"([^\"]*)\"");
		
			for (String s : values) {
				if (s == null)
					continue;
				Matcher m = p.matcher(s);
				if (m.find()) {

					String id = m.group(2);
					String dealprice = m.group(3);
					String shortname = m.group(1);
					Element outelement = outroot.addElement("Symbol");
					
					String exhno="";
					String tempid;
					if (htstockixmtypetoexh.containsKey(id)){
						tempid = htstockixmtypetoexh.get(id).getStock();
					    exhno = htstockixmtypetoexh.get(id).getExhno();
					    id = tempid;
					}
					else if (htstockexh.containsKey(id)){
						List<String> listexh = htstockexh.get(id);
						if (listexh.size() >1){
							exhno=listexh.get(0);
							listexh.remove(0);
							htstockexh.replace(id, listexh);
						}else{
							exhno = listexh.get(0);
						}
						if (htFMSTMB.containsKey(exhno+id))
							shortname=htFMSTMB.get(exhno+ id).getCname();
					}
					
					
					
					outelement.addAttribute("id", id);
					outelement.addAttribute("dealprice", dealprice);
					outelement.addAttribute("shortname", shortname);
					outelement.addAttribute("exhno", exhno);
					if (htcheckstock.containsKey(id+"|"+exhno)) {
						htcheckstock.remove(id+"|"+exhno);
					}
				}

			}
			
			//無法從redis查回資料,需從預載商品取得
			for (Enumeration e=htcheckstock.keys(); e.hasMoreElements();) {
				String nextKey = (String) e.nextElement();
				String[]  stockarr=nextKey.split("\\|");
				String id=stockarr[0];
				String dealprice = "0";
				String shortname = stockarr[0];
				Element outelement = outroot.addElement("Symbol");
				String exhno=stockarr[1];
				if (htFMSTMB.get(exhno+ id)!=null){
					shortname = htFMSTMB.get(exhno+ id).getCname();
					dealprice = htFMSTMB.get(exhno+ id).getCprice();
				}
				
				
				
				outelement.addAttribute("id", id);
				outelement.addAttribute("dealprice", dealprice);
				outelement.addAttribute("shortname", shortname);
				outelement.addAttribute("exhno", stockarr[1]);
			}
			
    	}
    	else{
    		logger.error("NO JEDIS CONNECTION");
    		dayLogger.error("NO JEDIS CONNECTION");
    	}
    	long ed=System.currentTimeMillis();
    	return result = outdoc.asXML();
    	
    }

    public static  Element getCacheInfo(String key){
    	Element  info=null;
    	if (htPriceInfo.containsKey(key)){
    		PriceInfo priceInfo=htPriceInfo.get(key);
    		info = priceInfo.getInfo();
    	}
    	return info;
    }
    
    public static  void setCacheInfo(String key,Element element){
    	if (htPriceInfo.containsKey(key)){
    		
    		
    		try {
    			PriceInfo priceInfo=new PriceInfo();
				priceInfo.setInfo(element);
				Calendar cal=Calendar.getInstance();
	    		priceInfo.setActivetime(cal.getTimeInMillis());
	    		htPriceInfo.replace(key, priceInfo);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    	}
    	else{
    		
    		try {
    			PriceInfo priceInfo=new PriceInfo();
				priceInfo.setInfo(element);
				Calendar cal=Calendar.getInstance();
	    		priceInfo.setActivetime(cal.getTimeInMillis());
	    		htPriceInfo.put(key, priceInfo);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    	}
    }
    
    public static void getALLPrice(){
    	Jedis jedis = RedisPoolManager.getResource();
    	long st=System.currentTimeMillis();
    	if (jedis!=null){
    		String[] searchKyes=new String[]{"1,*","8,*"};
    		for(int i=0;i < searchKyes.length ;i++){
    			Set<String> names = jedis.keys(searchKyes[i]);
    			String[] keys=names.toArray(new String[names.size()]);
    			List<String> values=jedis.mget(keys);
    			Document document;
    			
    			for(String value : values){
    				try {
    					document = DocumentHelper.parseText(value);
    					Element rootElt = document.getRootElement(); 
    					String id=rootElt.attribute("id").getValue();
    					String dealprice=rootElt.attribute("dealprice").getValue();
    					String shortname=rootElt.attribute("shortname").getValue();
    					if (htMSTMB.containsKey(id)){
    						shortname = htMSTMB.get(id).cname;
    					}
    					Document outdoc = DocumentHelper.createDocument();
    					Element outelement=outdoc.addElement("Symbol");
    					outelement.addAttribute("id", id);
    					outelement.addAttribute("dealprice", dealprice);
    					outelement.addAttribute("shortname", shortname);
    					setCacheInfo(id, outelement);
    				} catch (DocumentException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
    			}
    			
    			

    		}
    		long ed=System.currentTimeMillis();
    		System.out.println(ed- st);
    		
    	}
    }
    
    
    
    private static void loadMAPPINGLISTInfo(){
    	String connectionString =  
                "jdbc:sqlserver://"+ Conf.DB_IP+":"+Conf.DB_PORT+";"  
                + "database="+ Conf.FDB_NAME+";"  
                + "user="+Conf.DB_USER+";"  
                + "password="+Conf.DB_PWD+";"  
                + "loginTimeout=30;";  

            // Declare the JDBC objects.  
	    	Connection connection = null;  
	        Statement statement = null;   
	        ResultSet rs = null;  
	        java.util.Hashtable<String, MAPPINGLISTInfo> httempMAPPINGList=new java.util.Hashtable<String, MAPPINGLISTInfo>();      
            try {
            	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                connection = DriverManager.getConnection(connectionString);  
                
                String selectSql = "SELECT EXHNO, STOCK, STOCKMAPPING  FROM MAPPINGLIST";  
                statement = connection.createStatement();  
                rs = statement.executeQuery(selectSql); 
                while(rs.next()){
                	MAPPINGLISTInfo mappinglist=new MAPPINGLISTInfo();
                	String exhno = rs.getString("EXHNO").trim();
                	String stock = rs.getString("STOCK").trim();
                	String stockmapping = rs.getString("STOCKMAPPING").trim();
                	String key = exhno+stock;
                	
                	mappinglist.setExhno(exhno);
                	mappinglist.setStock(stock);
                	mappinglist.setStockmapping(stockmapping);
                	
                	if (!httempMAPPINGList.containsKey(key)){
                		httempMAPPINGList.put(key, mappinglist);
                	}
                }
                
                if (httempMAPPINGList.size()>0){
                	htMAPPINGList = httempMAPPINGList;
                }
                logger.info("MAPPINGList length="+htMAPPINGList.size());
            }  
            catch (Exception ex) {  
                ex.printStackTrace();
                logger.error(ex);
        		dayLogger.error(ex);
            }  
            finally {  
            	if (rs!=null){
	            	try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
            	if (statement!=null){
            		try {
						statement.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
                if (connection != null) try { connection.close(); } catch(Exception e) {}  
            }  
    }
    

    
    public static void reloadInfo(String itype) throws Exception{
        try{
        	ITypeEnum itypeEnum= ITypeEnum.valueOf(itype);
        	
        	switch(itypeEnum){
        	    case MAPPINGLIST:
        	    	logger.info("Reload MAPPINGLIST....");
        			dayLogger.info("Reload MAPPINGLIST....");
        	    	loadMAPPINGLISTInfo();
        	    	break;
        	    default:
        	    	throw new Exception("itype "+ itype +" no support");
        	    	
        	}
        }
    	catch(Exception ex){
    	    throw ex;
    	}
    }
    

}
