package com.systex;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import redis.clients.jedis.Jedis;
import org.apache.log4j.*;

public class SGTPPriceServlet extends HttpServlet implements Runnable {
	Thread thread;
	Logger logger = Logger.getLogger(SGTPPriceServlet.class);
	Logger dayLogger = Logger.getLogger("DAY");
    private String loadtime;
    private String loadeddate;
    private boolean reloaded=false;
	public void init() throws ServletException {
		logger.info("SGTPPriceServlet init");
		dayLogger.info("SGTPPriceServlet init");
		
		
		SGTPPrice.init();
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
    	Calendar cal=Calendar.getInstance();
		loadeddate =  df.format(cal.getTime());
		
		loadtime = getInitParameter("LOADTIME");
		logger.info("loadtime="+loadtime);
		dayLogger.info("loadtime="+loadtime);
		thread=new Thread(this);
		thread.start();
	}

	public void run() {
		
		while(true){
			try {
				DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		    	Calendar cal=Calendar.getInstance();
		    	String datetimestr = df.format(cal.getTime());
		    	String nowDate = datetimestr.substring(0, 8);
		    	String nowTime = datetimestr.substring(8);
		    	
		    	
		    	int nowtimecmp=Integer.parseInt(nowTime);
		    	int loadtimecmp=Integer.parseInt(loadtime);
		    	
		    	if (!loadeddate.equals(nowDate)){
		    		reloaded = false;
		    	}
		    	
		    	if (!reloaded && (nowtimecmp>=loadtimecmp && nowtimecmp<=loadtimecmp+300) ){
		    		logger.info("reload.. ");
		    		dayLogger.info("reload.. ");
		    		SGTPPrice.init();
		    		loadeddate= nowDate;
		    		reloaded=true;
		    	}
		    	
				thread.sleep(1000 * 60);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// TODO Auto-generated method stub
//		Jedis jedis = RedisPoolManager.getResource();
//		Subscriber subscriber = new Subscriber();
//		jedis.subscribe(subscriber, "TW_PRICE");
//		System.out.println("Subscriber");
		// while (true) {
		//
		// try {
		// System.out.println("load price " +
		// java.util.Calendar.getInstance().getTime());
		// SGTPPrice.getALLPrice();
		// searcher.sleep(3000);
		// }
		// catch (InterruptedException ignored) { }
		// }

	}
	 
	 
}
