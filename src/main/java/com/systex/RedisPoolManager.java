package com.systex;

import java.util.*;
import java.lang.*;
import java.io.*;


import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


public class RedisPoolManager {
	private static JedisPool pool = null;
    private static JedisPool getPool() {
        if (pool == null) {

        	
        	
            JedisPoolConfig config = new JedisPoolConfig();
            config.setMaxTotal(Conf.MAX_TOTAL);
            config.setMaxIdle(Conf.MAX_IDLE);
            config.setMaxWaitMillis(1000 * Conf.MAX_WAIT);
            config.setMinIdle(Conf.MIN_IDLE);
            //new JedisPool(config, ADDR, PORT, TIMEOUT, AUTH);
            config.setTestOnBorrow(false);
            config.setTestOnReturn(true);
            pool = new JedisPool(config, Conf.REDIS_IP, Conf.REDIS_PORT, 10000, Conf.AUTH_USER);

        }
        return pool;
    }

    public synchronized static Jedis getResource() {
        if (pool == null) {
            pool = getPool();
        }
        return pool.getResource();
    }	
	

}