package com.systex;

public class MAPPINGLISTInfo {
	public String stock;
	public String exhno;
	public String stockmapping;
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public String getExhno() {
		return exhno;
	}
	public void setExhno(String exhno) {
		this.exhno = exhno;
	}
	public String getStockmapping() {
		return stockmapping;
	}
	public void setStockmapping(String stockmapping) {
		this.stockmapping = stockmapping;
	}
}
