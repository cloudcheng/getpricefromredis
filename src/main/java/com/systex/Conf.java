package com.systex;


import java.util.*;
import java.lang.*;
import java.io.*;
import org.apache.log4j.*;

public class Conf {
	public static String REDIS_IP;
	public static String AUTH_USER = "systex";
	public static int REDIS_PORT;
	public static int MAX_TOTAL = 10;
	public static int MAX_IDLE = 5;
	public static int MIN_IDLE  = 5;
	public static int MAX_WAIT  = 10;
	public static String DB_IP;
	public static String DB_PORT;
	public static String DB_NAME;
	public static String FDB_NAME;
	public static String DB_USER;
	public static String DB_PWD;
	public static String DB_DRIVER_TYPE;
	
	public static String MARKET_TW_STOCK_ENABLE;
	public static String MARKET_FOREIGN_STOCK_ENABLE;
	
	Logger logger=Logger.getLogger(Conf.class);
	public Conf(){
	}
	
	public void load(){
		Properties properties = new Properties();
		try {
			properties.load(this.getClass().getClassLoader().getResourceAsStream("getprice.properties"));
			this.REDIS_IP = properties.getProperty("REDIS_IP");
			this.REDIS_PORT = Integer.parseInt(properties.getProperty("REDIS_PORT"));
			this.MAX_TOTAL = Integer.parseInt(properties.getProperty("MAX_TOTAL"));
			this.MAX_IDLE = Integer.parseInt(properties.getProperty("MAX_IDLE"));
			this.MIN_IDLE = Integer.parseInt(properties.getProperty("MIN_IDLE"));
			this.MAX_WAIT = Integer.parseInt(properties.getProperty("MAX_WAIT"));
			this.AUTH_USER = properties.getProperty("AUTH_USER");
			this.DB_IP = properties.getProperty("DB_IP");
			this.DB_PORT = properties.getProperty("DB_PORT");
			this.DB_NAME = properties.getProperty("DB_NAME");
			this.FDB_NAME = properties.getProperty("FDB_NAME");
			this.DB_USER = properties.getProperty("DB_USER");
			this.DB_PWD = properties.getProperty("DB_PWD");
			this.DB_DRIVER_TYPE = properties.getProperty("DB_DRIVER_TYPE");
			this.MARKET_TW_STOCK_ENABLE = properties.getProperty("MARKET_TW_STOCK_ENABLE");
			this.MARKET_FOREIGN_STOCK_ENABLE = properties.getProperty("MARKET_FOREIGN_STOCK_ENABLE");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
			
		}
	}
}
