package com.systex;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Document;
import org.dom4j.DocumentException;

import redis.clients.jedis.JedisPubSub;

public class  Subscriber extends JedisPubSub implements Runnable {
	Thread thread;
	public Subscriber(){
		thread = new Thread(this);
		thread.start();
	}
	@Override
    public void onMessage(String channel, String message) {
//	    System.out.println("Message received. Channel:"+channel +", Msg:"+ message);
		AddPriceChange(message);
    }

	@Override
    public void onPMessage(String pattern, String channel, String message) {

	}

	@Override
    public void onSubscribe(String channel, int subscribedChannels) {

	}

	@Override
    public void onUnsubscribe(String channel, int subscribedChannels) {

	}

	@Override
    public void onPUnsubscribe(String pattern, int subscribedChannels) {

	}

    @Override
	public void onPSubscribe(String pattern, int subscribedChannels) {

	}

    public void AddPriceChange(String msg){
    	if (msg!=null){
    		synchronized(SGTPPrice.priceChangeList){
    			SGTPPrice.priceChangeList.add(msg);
    		}

    	}
    }
	public void run() {
		// TODO Auto-generated method stub
		String[] priceInfos=null;
		try {
			while(true){
				synchronized(SGTPPrice.priceChangeList){
	            	if (SGTPPrice.priceChangeList.size() > 0){
	            		priceInfos=SGTPPrice.priceChangeList.toArray(new String[SGTPPrice.priceChangeList.size()]);
	            		SGTPPrice.priceChangeList.clear();
	            	}
	            }
				
				if (priceInfos!=null && priceInfos.length > 0){
					System.out.println("queue size="+ priceInfos.length);
					for(String info : priceInfos){
						Document document;
						try {
							document = DocumentHelper.parseText(info);
							Element rootElt = document.getRootElement(); 
							String id=rootElt.attribute("id").getValue();
							String dealprice=rootElt.attribute("dealprice").getValue();
							String shortname=rootElt.attribute("shortname").getValue();
							Document ouputdocument = DocumentHelper.createDocument();
							Element symbolElt=ouputdocument.addElement("Symbol");
							symbolElt.addAttribute("id", id);
							symbolElt.addAttribute("dealprice", dealprice);
							symbolElt.addAttribute("shortname", shortname);
							SGTPPrice.setCacheInfo(id,symbolElt);
							
						} catch (DocumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
					}
					priceInfos=null;
				}
	            
	            thread.sleep(50);
			}
			
		} catch (InterruptedException ignored) {
		}
	}

}
